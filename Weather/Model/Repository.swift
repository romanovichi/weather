//
//  CurrentWeatherRepository.swift
//  Weather
//
//  Created by Иван Романович on 13.08.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class Repository<T: WeatherObject> where T: Object {
    
    let realm = try! Realm()
    
    func save(item: T) {
        try? realm.write {
            realm.add(item, update: true)
        }
    }
    
    func getItemsFromDatabase() -> [T] {
        return realm.objects(T.self).map { $0 }
    }
}

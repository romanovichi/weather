//
//  CurrentWeather.swift
//  Wether
//
//  Created by Иван Романович on 18.07.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

protocol WeatherObject {
    func getUpdateTime() -> Double
    static func from<T: Object>(_ json: JSON) -> T
}

class CurrentWeather: Object, WeatherObject {
    
    @objc dynamic var id = 0
    @objc dynamic var updateTime: Double = Date().timeIntervalSince1970
    @objc dynamic var name: String = ""
    @objc dynamic var temp: Int = 0
    @objc dynamic var iconName: String = ""
    
    convenience init(_ name: String, _ temp: Int, _ iconName: String) {
        self.init()
        self.name = name
        self.temp = temp
        self.iconName = iconName
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

extension CurrentWeather {
    
    func getWeatherIcon() -> UIImage? {
        if iconName == "03d" || iconName == "03n" || iconName == "04d" || iconName == "04n" {
            return UIImage(named: "03")
        }
        if iconName == "11d" || iconName == "11n" {
            return UIImage(named: "11")
        }
        return UIImage(named: iconName)
    }
    
    func getUpdateTime() -> Double {
        return updateTime
    }
    
    static func from<T>(_ json: JSON) -> T where T : Object {
        let name = json["name"].stringValue
        let temp = json["main"]["temp"].intValue
        let weatherArray = json["weather"].arrayValue
        var iconName = "none"
        if let weather = weatherArray.first {
            iconName = weather["icon"].stringValue
        }
        let currentWeather = CurrentWeather(name, temp, iconName)
        return currentWeather as! T
    }
}

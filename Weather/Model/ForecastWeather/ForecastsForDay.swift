//
//  Day.swift
//  Wether
//
//  Created by Иван Романович on 23.07.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import RealmSwift

class ForecastsForDay: Object {
    
    @objc dynamic var name: String = ""
    @objc dynamic var minTemp: Int = 0
    @objc dynamic var maxTemp: Int = 0
    
    convenience init(name: String, minTemp: Int, maxTemp: Int) {
        self.init()
        self.name = name
        self.minTemp = minTemp
        self.maxTemp = maxTemp
    }
}

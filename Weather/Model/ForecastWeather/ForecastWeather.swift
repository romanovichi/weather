//
//  ForecastWeather.swift
//  Wether
//
//  Created by Иван Романович on 19.07.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class ForecastWeather: Object, WeatherObject {

    @objc dynamic var id = 0
    @objc dynamic var updateTime: Double = Date().timeIntervalSince1970
    var list = List<ForecastsForDay>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

extension ForecastWeather {
    
    func getUpdateTime() -> Double {
        return updateTime
    }
    
    static func from<T>(_ json: JSON) -> T where T : Object {
        let weatherData = json["list"].arrayValue
        let forecast = ForecastWeather()
        
        var seen: [DateComponents: Bool] = [:]
        let uniqueDates = weatherData.compactMap { Date(timeIntervalSince1970: $0["dt"].doubleValue) }
            .filter { !Calendar.current.isDateInToday($0) }
            .compactMap { Calendar.current.dateComponents([.day, .month, .year], from: $0) }
            .filter { seen.updateValue(true, forKey: $0) == nil }
            .compactMap { Calendar.current.date(from: $0) }
        
        uniqueDates.forEach {
            let date = $0
            let dateComponents = Calendar.current.dateComponents([.day, .month, .year], from: date)
            
            let forecastsForThisDay = weatherData.filter {
                
                let comparingDate = Date(timeIntervalSince1970: $0["dt"].doubleValue)
                let comparingDateComponents = Calendar.current.dateComponents([.day, .month, .year], from: comparingDate)
                
                return dateComponents == comparingDateComponents
            }
            
            if let min = (forecastsForThisDay.compactMap { $0["main"]["temp_min"].int }).min(),
                let max = (forecastsForThisDay.compactMap { $0["main"]["temp_max"].int }).max() {
                forecast.list.append(ForecastsForDay(name: date.getWeekdayName, minTemp: min, maxTemp: max))
            }
        }
        return forecast as! T
    }
}

//
//  DataSource.swift
//  Weather
//
//  Created by Иван Романович on 10.08.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import RealmSwift

class DataService<T: WeatherObject> where T: Object {

    let repository = Repository<T>()
    let apiClient = ApiClient()
    
    var weather = [T]()
}

extension DataService {

    func getWeather(_ completion: @escaping (T) -> ()) {
        weather = repository.getItemsFromDatabase()
        
        if weather.isEmpty || timeSinceLastUpdateWeather() > Time.thirtyMinutes {
            getWeatherFromApi {
                self.repository.save(item: $0)
                completion($0)
            }
        } else {
            completion(weather.first!)
        }
    }

    private func getWeatherFromApi(_ completion: @escaping (T)->()) {
        
        apiClient.getWeatherJson(type: T.self) { (json) in
            let weather = T.from(json)
            completion(weather as! T)
        }
    }

    func timeSinceLastUpdateWeather() -> Double {
        if let weather = weather.first {
            return Date().timeIntervalSince1970 - weather.getUpdateTime()
        }
        return Double()
    }
}

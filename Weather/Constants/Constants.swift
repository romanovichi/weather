//
//  Constants.swift
//  Wether
//
//  Created by Иван Романович on 16.07.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation

struct Urls {
    
    static let currentWeatherUrl = "http://api.openweathermap.org/data/2.5/weather?id=1489425&units=metric&APPID=8e02f0df21b5114cfdfaa6246633851f"
    static let forecastWeatherUrl = "http://api.openweathermap.org/data/2.5/forecast?id=1489425&units=metric&APPID=8e02f0df21b5114cfdfaa6246633851f"
}

struct Cells {
    
    static let firstCell = 0
}

struct Time {
    
    static let thirtyMinutes: Double = 30 * 60
    static let oneHour: Double = 60 * 60
}

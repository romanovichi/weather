//
//  API.swift
//  Wether
//
//  Created by Иван Романович on 16.07.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum Url: String {
    case current = "http://api.openweathermap.org/data/2.5/weather?id=1489425&units=metric&APPID=8e02f0df21b5114cfdfaa6246633851f"
    case forecast = "http://api.openweathermap.org/data/2.5/forecast?id=1489425&units=metric&APPID=8e02f0df21b5114cfdfaa6246633851f"
}

class ApiClient {
    
    func getWeatherJson<T: WeatherObject>(type: T.Type, completion: @escaping (JSON)->()) {
        
        var url: Url = .current
        
        switch type {
        case is CurrentWeather.Type:
            url = .current
        case is ForecastWeather.Type:
            url = .forecast
        default:
            print("Error in ApiClient: there is no such type")
        }
        
        Alamofire.request(url.rawValue, method: .get).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else { return }
                if let json = try? JSON(data: data) {
                    completion(json)
                }
            } else {
                print("Error in getWeatherData: \(String(describing: response.result.error))")
            }
        }
    }
}

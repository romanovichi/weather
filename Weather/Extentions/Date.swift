//
//  Double.swift
//  Wether
//
//  Created by Иван Романович on 19.07.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import Foundation

extension Date {
    
    var getWeekdayName: String {
        let dateFormatter = DateFormatter()
        let weekday = dateFormatter.weekdaySymbols[Calendar.current.component(.weekday, from: self)-1]
        return weekday
    }
    
    var getNextDay: Date {
        return Calendar.current.date(byAdding: .weekday, value: 1, to: self)!
    }
}

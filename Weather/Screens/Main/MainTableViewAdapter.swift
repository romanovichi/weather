//
//  MainTableViewAdapter.swift
//  Wether
//
//  Created by Иван Романович on 18.07.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit

class MainTableViewAdapter: NSObject {

    private let table: UITableView!
    
    private var rowsForCurrentWeather = 1
    private var rowsForForecastWeather = 0
    
    var currentWeather = CurrentWeather()
    var forecast = ForecastWeather()
    
    init(classTable: UITableView) {
        
        table = classTable
        
        table.registerCell(.weatherCell)
        table.registerCell(.currentWeatherCell)
        table.allowsSelection = false
        
        super.init()
        
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 100
        table.tableFooterView = UIView()
        
        table.delegate = self
        table.dataSource = self
    }
}

extension MainTableViewAdapter {
    
    private func configureRows() -> Int {
        if !forecast.list.isEmpty {
            rowsForForecastWeather = forecast.list.count
        }
        return rowsForCurrentWeather + rowsForForecastWeather
    }
    
    func updateCurrentWeather(with current: CurrentWeather) {
        self.currentWeather = current
        table.reloadData()
    }
    
    func updateForecastWeather(with forecast: ForecastWeather) {
        self.forecast = forecast
        table.reloadData()
    }
}

extension MainTableViewAdapter: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return configureRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == Cells.firstCell {
            if let cell = tableView.dequeueReusableCell(of: .currentWeatherCell, for: indexPath) as? CurrentWeatherCell {
                cell.set(item: currentWeather)
                return cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(of: .weatherCell, for: indexPath) as? WeatherCell {
                cell.set(item: forecast.list[indexPath.row - rowsForCurrentWeather])
                return cell
            }
        }
        return UITableViewCell()
    }
}

//
//  WeatherCell.swift
//  Wether
//
//  Created by Иван Романович on 18.07.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {
    
    @IBOutlet weak var weekdayLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    
    private var item: ForecastsForDay! {
        didSet {
            weekdayLabel.text = item.name
            maxTempLabel.text = "\(item.maxTemp)"
            minTempLabel.text = "\(item.minTemp)"
        }
    }
    
    func set(item: ForecastsForDay) {
        self.item = item
    }
}

//
//  CurrentWeatherCell.swift
//  Weather
//
//  Created by Иван Романович on 27.07.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit

class CurrentWeatherCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    private var item: CurrentWeather! {
        didSet {
            iconImageView.image = item.getWeatherIcon()
            cityNameLabel.text = item.name
            tempLabel.text = ("\(item.temp)")
        }
    }
    
    func set(item: CurrentWeather) {
        self.item = item
    }
}

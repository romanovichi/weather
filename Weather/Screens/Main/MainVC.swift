//
//  MainVC.swift
//  Wether
//
//  Created by Иван Романович on 16.07.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import UIKit

class MainVC: BaseVC {
    
    @IBOutlet weak var tableView: UITableView!
    
    private var adapter: MainTableViewAdapter!
    var refresher: UIRefreshControl!
    let currentWeatherDataService = DataService<CurrentWeather>()
    let forecastWeatherDataService = DataService<ForecastWeather>()
    private var isLoading: Bool = false {
        didSet {
            if oldValue {
                refresher.endRefreshing()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adapter = MainTableViewAdapter(classTable: tableView)
        setupRefresher()
        updateWeather()
    }
}

extension MainVC {
    
    func setupRefresher() {
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(updateWeather), for: .valueChanged)
        tableView.addSubview(refresher)
    }
    
    @objc func updateWeather() {
        isLoading = true
        currentWeatherDataService.getWeather {
            self.adapter.updateCurrentWeather(with: $0)
            self.isLoading = false
        }
        forecastWeatherDataService.getWeather {
            self.adapter.updateForecastWeather(with: $0)
            self.isLoading = false
        }
    }
}

//
//  WeatherTests.swift
//  WeatherTests
//
//  Created by Иван Романович on 25.07.2018.
//  Copyright © 2018 Иван Романович. All rights reserved.
//

import XCTest
@testable import Weather

class WeatherTests: XCTestCase {
    
    func testGetWeekdayName() {
        // given
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: "2018-07-25 15:53:30")
        // when
        let weekdayName = date?.getWeekdayName
        // then
        XCTAssertEqual(weekdayName, "Wednesday")
    }
    
    func testGetNextDay() {
        // given
        let date = Date(timeIntervalSince1970: 1532509200.0)
        // when
        let nextDate = date.getNextDay
        // then
        XCTAssertEqual(nextDate, Date(timeIntervalSince1970: 1532595600.0))
    }
    
}
